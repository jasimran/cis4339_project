class RemoveAppointmentTimeFromAppointments < ActiveRecord::Migration
  def change
    remove_column :appointments, :appointment_time, :datetime
  end
end

class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :patient_id
      t.integer :physician_id
      t.integer :diagnostic_code_id
      t.datetime :appointment_time
      t.string :appointment_reason
      t.string :appointment_status
      t.string :physician_note
      t.decimal :fee


      t.timestamps
    end
  end
end

class AddAddressToPhysicians < ActiveRecord::Migration
  def change
    add_column :physicians, :address, :string
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

TimeSlot.create(start_time: '8:00AM', end_time: '8:30AM' )
TimeSlot.create( start_time: '8:30AM', end_time: '9:00AM' )
TimeSlot.create( start_time: '9:00AM', end_time: '9:30AM' )
TimeSlot.create(start_time: '9:30AM', end_time: '10:00AM' )
TimeSlot.create(start_time: '10:00AM', end_time: '10:30AM' )
TimeSlot.create( start_time: '10:30AM', end_time: '11:00AM' )
TimeSlot.create( start_time: '11:00AM', end_time: '11:30AM' )
TimeSlot.create( start_time: '11:30AM', end_time: '12:00PM' )
TimeSlot.create(start_time: '12:00PM', end_time: '12:30PM' )
TimeSlot.create( start_time: '12:30PM', end_time: '1:00PM' )
TimeSlot.create( start_time: '1:00PM', end_time: '1:30PM' )
TimeSlot.create( start_time: '1:30PM', end_time: '2:00PM' )
TimeSlot.create(start_time: '2:00PM', end_time: '2:30PM' )
TimeSlot.create( start_time: '2:30PM', end_time: '3:00PM' )
TimeSlot.create( start_time: '3:00PM', end_time: '3:30PM' )
TimeSlot.create( start_time: '3:30PM', end_time: '4:00PM' )
TimeSlot.create( start_time: '4:00PM', end_time: '4:30PM' )


Patient.create(name: 'John Maxwell', address: '322 Holland Drive', phone: '2813560983')
Patient.create(name: 'Tom Thumb', address: '45 Rampart Rd', phone: '2813429983')
Patient.create(name: 'Bill Todd', address: '6895 Broadway', phone: '2818839686')

Physician.create(name: 'Harold Harper', address: '56 Downhill Rd', phone: '2818869686', status: 'Active')
Physician.create(name: 'James Jr', address: '69 Uphill Rd', phone: '2811389645', status: 'Active')

DiagnosticCode.create(dermatology_code: 'Patient Undiagnosed')


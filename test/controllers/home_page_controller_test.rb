require 'test_helper'

class HomePageControllerTest < ActionController::TestCase
  test "should get admin" do
    get :admin
    assert_response :success
  end

  test "should get physician" do
    get :physician
    assert_response :success
  end

  test "should get patient" do
    get :patient
    assert_response :success
  end

  test "should get office_worker" do
    get :office_worker
    assert_response :success
  end

end

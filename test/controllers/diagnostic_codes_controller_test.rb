require 'test_helper'

class DiagnosticCodesControllerTest < ActionController::TestCase
  setup do
    @diagnostic_code = diagnostic_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:diagnostic_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create diagnostic_code" do
    assert_difference('DiagnosticCode.count') do
      post :create, diagnostic_code: { dermatology_code: @diagnostic_code.dermatology_code }
    end

    assert_redirected_to diagnostic_code_path(assigns(:diagnostic_code))
  end

  test "should show diagnostic_code" do
    get :show, id: @diagnostic_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @diagnostic_code
    assert_response :success
  end

  test "should update diagnostic_code" do
    patch :update, id: @diagnostic_code, diagnostic_code: { dermatology_code: @diagnostic_code.dermatology_code }
    assert_redirected_to diagnostic_code_path(assigns(:diagnostic_code))
  end

  test "should destroy diagnostic_code" do
    assert_difference('DiagnosticCode.count', -1) do
      delete :destroy, id: @diagnostic_code
    end

    assert_redirected_to diagnostic_codes_path
  end
end

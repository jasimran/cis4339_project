require 'test_helper'

class TimeSlotTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @time_slot = TimeSlot.new
  end

  test "Time_slot_difference_is_30_min" do
    @time_slot.start_time = Time.parse("8:30AM")
    @time_slot.end_time = Time.parse("9:30AM")
    assert_match /for appointment has to be 30 min after the appointment start time/, @time_slot.chk_slot_time_30_min.to_s
  end

  test "Time_slot is before 8 am " do
    @time_slot.start_time = Time.parse("7:30AM")

    assert_match /cannot be before 8:00AM/, @time_slot.chk_time_slot_before_8am.to_s
  end

  test "Time_slot is after 4_30pm" do
    @time_slot.end_time = Time.parse("5:30PM")
    assert_match /cannot be after 4:30PM/, @time_slot.chk_time_slot_after_4_30pm.to_s
  end

end


require 'test_helper'

class AppointmentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @appointment = Appointment.new
  end


  test "appointment days is not on Sat or Sun" do
    @appointment.appointment_date = Date.parse('2015-02-07')
    assert_match /No appointments on Sat or Sun/, @appointment.chk_appointment_day_not_sat_sun.to_s

  end

  test "Cannot have an appointment before current date" do
    @appointment.appointment_date = Date.today - 2.day
    assert_match /cannot be today/, @appointment.chk_appointment_date_greater_than_today.to_s

  end

  test "no_appointment_overlap" do
    appointment1 = Appointment.create(:time_slot_id => 1,:physician_id => 1, :appointment_date => '2015-02-10' )
    assert appointment1.valid?, "Appointment1 was not valid #{appointment1.errors.inspect}"

    appointment2 = Appointment.new(:time_slot_id => appointment1.time_slot_id,
                                   :physician_id => appointment1.physician_id,
                                  :appointment_date => appointment1.appointment_date)
    appointment2.valid?
   assert_not_nil appointment2.errors.get(:time_slot_id)
  end


end

json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :patient_id, :physician_id, :diagnostic_code_id, :appointment_date, :appointment_reason, :appointment_status, :physician_note, :fee
  json.url appointment_url(appointment, format: :json)
end

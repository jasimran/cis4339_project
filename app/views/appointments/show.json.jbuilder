json.extract! @appointment, :id, :patient_id, :physician_id, :diagnostic_code_id, :appointment_date, :appointment_reason, :appointment_status, :physician_note, :fee, :created_at, :updated_at

class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @appointments = Appointment.all
    respond_with(@appointments)
  end

  def show
    respond_with(@appointment)
  end

  def new
    @appointment = Appointment.new
    respond_with(@appointment)
  end

  def edit
  end


  def create

    @appointment = Appointment.new(appointment_params)
    @appointment.save
    respond_with(@appointment)
  end

  def update
    @appointment.update(appointment_params)
    respond_with(@appointment)
  end

  def destroy
    @appointment.destroy
    respond_with(@appointment)
  end

  def patient_show
    @appointment = Appointment.find(params[:id])
    @patient = Patient.find(params[:id])
    respond_with(@appointment)
  end

  def patient_index
    @patients = Patient.all
    @appointment = Appointment.find(params[:id])
    respond_with(@appointment)
    @appointment = Appointment.new(appointment_params)
    @appointment.save
    respond_with(@appointment)
  end

  def save_patient_index

    @appointment = Appointment.new(appointment_params)
    @appointment.save
    respond_with(@appointment)
  end


  private


    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    def appointment_params
      params.require(:appointment).permit(:patient_id, :physician_id, :diagnostic_code_id, :appointment_reason, :appointment_status, :physician_note, :fee, :time_slot_id, :appointment_date)
    end


end

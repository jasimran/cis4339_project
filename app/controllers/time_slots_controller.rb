class TimeSlotsController < ApplicationController
  before_action :set_time_slot, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @time_slots = TimeSlot.all
    respond_with(@time_slots)
  end

  def show
    respond_with(@time_slot)
  end

  def new
    @time_slot = TimeSlot.new
    respond_with(@time_slot)
  end

  def edit
  end

  def create
    @time_slot = TimeSlot.new(time_slot_params)
    @time_slot.save
    respond_with(@time_slot)
  end

  def update
    @time_slot.update(time_slot_params)
    respond_with(@time_slot)
  end

  def destroy
    @time_slot.destroy
    respond_with(@time_slot)
  end

  private
    def set_time_slot
      @time_slot = TimeSlot.find(params[:id])
    end

    def time_slot_params
      params.require(:time_slot).permit(:start_time, :end_time)
    end
end

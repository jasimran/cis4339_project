class Appointment < ActiveRecord::Base
  belongs_to :physician
  belongs_to :patient
  belongs_to :diagnostic_code
  belongs_to :time_slot
  STATUS_TYPES = ["Pending", "Completed", "Cancelled", "Patient did not show"]

  validates_presence_of :appointment_date
  validates_uniqueness_of :time_slot_id, :scope => [:appointment_date, :physician_id]
  validate :chk_appointment_date_greater_than_today
  validate :chk_appointment_day_not_sat_sun
  before_destroy :chk_no_appointment_cancel_8hr




  def chk_no_appointment_cancel_8hr

    s_time = Time.now - self.time_slot.start_time
    if (self.appointment_date <= Date.today) and s_time < 8
      errors.add(:start_time, "is less than 8 hours before appointment. Contact dermatology office to cancel.")
    end
  end


def chk_appointment_date_greater_than_today
  if self.appointment_date < Date.today.end_of_day
    errors.add(:appointment_date, "cannot be today or before today.")
  end
end

  def chk_appointment_day_not_sat_sun
    #check so that appointment is not on Sat or Sun
    app_day = self.appointment_date.strftime("%a")
    check_day = self.appointment_date.wday
    if check_day == 0 or check_day == 6
      errors.add(:appointment_date,"you selected is #{app_day}. No appointments on Sat or Sun.")
    end
  end





end





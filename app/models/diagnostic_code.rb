class DiagnosticCode < ActiveRecord::Base
  has_many :appointments



  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
    DiagnosticCode.create! row.to_hash
      end
  end

  def get_completed_appointments
    where("appointment_status = ?", 'Completed')
    DiagnosticCode.joins(:appointments).where("appointment.appointment_status = 'Completed'")
  end


end

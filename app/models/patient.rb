class Patient < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments

  before_destroy :chk_no_appointment_cancel_8hr

  def getMin(time)
    return((time.hour*60) + time.min)
  end

  def chk_no_appointment_cancel_8hr
    Time.zone = 'Central Time (US & Canada)'
    s_time = getMin(self.appointments.time_slot.start_time - Time.zone.now)
    if self.appointments.appointment_date == Date.today() and s_time < (8.hour * 60)
      errors.add(:start_time, "is less than 8 hours before appointment. Contact dermatology office to cancel.")
    end
  end


end

class TimeSlot < ActiveRecord::Base
  has_many :appointments
  has_many :physicians, through: :appointments
  has_many :patients, through: :appointments

  validates_uniqueness_of :id, :scope => [:start_time, :end_time]

  def parsedStartTime
    "#{start_time.strftime("%H:%M%p")} - #{end_time.strftime("%H:%M%p")}"
  end


  validate :chk_time_slot_before_8am
  validate :chk_slot_time_30_min
  validate :chk_time_slot_after_4_30pm



  def chk_slot_time_30_min
    if (start_time.min - end_time.min) != 30
      errors.add( :end_time, "for appointment has to be 30 min after the appointment start time")
    end
  end

  def getMin(time)
    return((time.hour*60) + time.min)
  end

  def chk_time_slot_before_8am
    begin_t = Time.parse("7:59am")

    t_start_time = Time.parse(start_time.to_s)

    if (getMin(t_start_time) < getMin(begin_t))
      errors.add(:start_time, "cannot be before 8:00AM.")
    end
  end

  def chk_time_slot_after_4_30pm

    end_t = Time.parse("4:32pm")

    t_end_time = Time.parse(end_time.to_s)

    if  (getMin(t_end_time) > getMin(end_t))
      errors.add(:end_time, "cannot be after 4:30PM.")
    end
  end

end

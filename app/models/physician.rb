class Physician < ActiveRecord::Base
  has_many :appointments
  has_many :patients, through: :appointments
  STATUS_TYPES = ["Active", "Inactive"]


end

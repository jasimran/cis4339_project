Rails.application.routes.draw do
  get 'home_page/admin'

  get 'home_page/physician'

  get 'home_page/patient'

  get 'home_page/office_worker'

  get 'welcome/index'



  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"}
  resources :time_slots

  resources :diagnostic_codes

  resources :appointments

  resources :physicians

  resources :patients

  #get 'appointments/patient_index' => 'appointment#patient_index', as: :patient_index

  resources :appointments do
    member do
      post :patient_index
      get 'patient_index'
      post :save_patient_index
      get 'save_patient_index'
      post :patient_show
      get 'patient_show'
    end
  end

  resources :patients do
    member do
      post :patient_index
      get 'patient_index'
      post :save_patient_index
      get 'save_patient_index'
      post :patient_show
      get 'patient_show'
    end
  end

  resources :diagnostic_codes do
    collection { post :import }
  end



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
